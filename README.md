500px Gallery Downloader
======================
This simple script downloads a whole gallery from the photo hosting website [500px](https://500px.com) to your local disk. It can be used for backup purposes.

Features
--------
- Download a complete 500px gallery to local disk
- Download public and private galleries
- Incremental downloading which only downloads the new/missing images
- Can run headless, so without a graphical session, i.e. in a cron-job

Installation
------------
Make sure you have Google Chrome or Chromium installed and working. The script uses it to access 500px. It is not automatically installed.

```
# Clone this repository
git clone https://gitlab.com/theomega/500px-gallery-downloader.git
cd 500px-gallery-downloader

# Set up a virtual Env
python3 -m venv .

# Install the dependencies
./bin/pip install -r requirements.txt
```

Usage
-----
To download an gallery, you need to have the URL of the gallery page. This URL can be in different forms:

- Public Gallery: https://500px.com/alea777/galleries/abstract
- Private Gallery: https://500px.com/g/311a98d179da1b1219f58ff6fbccd62d615af5c42ef93989fc343b02cf4c955c

Provide this URL together with a target folder to the script:
```
./bin/python main.py --target ./abstract_photos/ https://500px.com/alea777/galleries/abstract
```

To download a private gallery, you do not need to provide any credentials, the "secret" URL of the gallery is enough
```
./bin/python main.py --target ./my_gallery/ https://500px.com/g/311a98d179da1b1219f58ff6fbccd62d615af5c42ef93989fc343b02cf4c955c
```

The downloading takes some time, so be patient.

Caveats
-------
- No sophisticated error checking
- Relies on the structure of the 500px webpage not changing
- There are some sleeps in the code, if you are on a slow internet connection, the sleeping time might not be enough for the website to load and render.
- The pictures have a maximum resolution of 2000px. This is a limtation of 500px and cannot be changed by this script.

Implementation
--------------
This script uses [Selenium](https://selenium-python.readthedocs.io/) and [chrome-webdriver](http://chromedriver.chromium.org) to simulate a user in a browser and discover the images in an gallery. The actual downloading is done using the [requests](http://docs.python-requests.org) python library. Chromedriver runs Google Chrome or Chromium in headless mode, so you are not seeing the browser window.

TODO
----
- Better error handling
- Better input validation of provided URL
- Replace single sleeps with better retry mechanism

Licence
-------
Apache Licence 2.0
