import time
from selenium import webdriver
import chromedriver_binary
import os
import requests
import shutil
import argparse
import logging
import sys

# Setup the command line argument parser
parser = argparse.ArgumentParser(description='Download a 500px gallery/album')
parser.add_argument('--debug', action='store_const',
                    const=True, default=False,
                    help="Print debug info")
parser.add_argument('--showbrowser', action='store_const',
                    const=True, default=False,
                    help="Show the headless browser window for debugging")
parser.add_argument('--target', '-t', required=True, help="Target Directory to store the downloaded images to")
parser.add_argument('gallery_url', metavar='GALLERY_URL', nargs=None, help="URL of a 500px gallery")

args = parser.parse_args()

# Setup Logging, disable some debug logging of dependencies
logging.basicConfig(level=logging.DEBUG if args.debug else logging.INFO)
logging.getLogger('selenium.webdriver').setLevel(logging.INFO)
logging.getLogger('urllib3').setLevel(logging.INFO)

logging.debug("Got arguments %s", args)

# Create Target Directory if it doesn't exist
if not os.path.exists(args.target):
  os.makedirs(args.target)

# Validate gallery URL
if not args.gallery_url.startswith('https://500px.com/'):
  logging.error('The gallery url provided does not look like a 500px URL')
  sys.exit(1)

# Create Chrome Session
options = webdriver.ChromeOptions()
# Don't show a Chrome Window
if not args.showbrowser:
  options.add_argument('headless')
# Make the window big enough to make the big images load
options.add_argument('window-size=2560x2560')
driver = webdriver.Chrome(options=options)

def get_photo_id_from_link(link):
  """Helper method to extract the photo id from a link to a photo detail page"""

  return link.split('/')[4]

def get_photo_links():
  """Helper method to extract the links to all photos from the currently active
  page. The active page should be a gallery listing"""

  photo_link_elements = driver.find_elements_by_css_selector('.photo_grid_region .photo_thumbnail .photo_link')
  links = set(map(lambda x: x.get_attribute('href'), photo_link_elements))
  logging.debug('Found %d links', len(links))
  return links

# Load the Gallery Listing Page
logging.info('Loading gallery from %s', args.gallery_url)
driver.get(args.gallery_url)

# Repeat scrolling to the bottom of the page until we are not finding any new
# links. This is done as 500px uses inifite scrolling and the script needs to be
# sure that all photos are visible.
# Get the Links, we need to capture the actual string, not the webdriver
# elements, otherwise we can't access it later.
photo_links = get_photo_links()
prev_photo_links = set()

# If we found new photos the last time, scroll another time to see if we can find more
while photo_links != prev_photo_links:
  logging.debug('Scrolling')
  driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")

  # Wait for the infinite scroll to finish loading
  while driver.find_element_by_css_selector('.infinite_scroll_container .infinite_scroll_loader').value_of_css_property('display') == 'block':
    time.sleep(0.1)

  prev_photo_links = photo_links
  photo_links = get_photo_links()

logging.info('Downloading %d photos', len(photo_links))

# Sort the links so we get a stable order
photo_links = sorted(photo_links)

# Iterate over each of the links we have collected earlier
for link in photo_links:
  # Extract the Photo ID from the link
  photo_id = get_photo_id_from_link(link)
  logging.debug('Found Photo %s', photo_id)

  # Check if we have the photo already as file, if yes skip
  target_file = os.path.join(args.target, photo_id + '.jpg')
  if os.path.exists(target_file):
    logging.info('Skipping Photo %s as it was already downloaded', photo_id)
    continue

  # Open the detail page of the piecture
  driver.get(link)
  logging.debug('Requesting photo detail page at %s', link)

  # Wait for the image to load
  while  len(driver.find_elements_by_css_selector('img.photo-show__img')) == 0:
    time.sleep(0.1)

  # Get the High-Res Version
  src = driver.find_element_by_css_selector('img.photo-show__img').get_attribute('src')
  try_count = 50

  # If this is not a high-res version, retry
  while 'm%3D2000' not in src and 'm%3D1000' not in src:
    logging.debug('Didnt find full res image for photo %s (got %s), waiting', photo_id, src)
    time.sleep(0.1)
    src = driver.find_element_by_css_selector('img.photo-show__img').get_attribute('src')
    try_count = try_count - 1

    if (try_count == 0):
        # Have an upper limit for how long to wait, maybe there is not high res
        # version of the image.
        logging.warning('Giving up finding the high res version for photo %s', photo_id)
        break

  # We have an URL, download it
  r = requests.get(src, stream=True)
  r.raise_for_status()
  with open(target_file, 'wb') as f:
      r.raw.decode_content = True
      shutil.copyfileobj(r.raw, f) 
      logging.info('Downloaded photo %s from %s to %s', photo_id, src, target_file)

# Exit the Chrome Session
driver.quit()
